package com.moresise;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by Work on 05.08.2016.
 */

@Component
public class JsonParser {
    ObjectMapper mapper = new ObjectMapper();

    public Payaccount convret(String json) throws IOException {
        return mapper.readValue(json, Payaccount.class);
    }
}
