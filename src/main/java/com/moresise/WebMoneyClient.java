package com.moresise;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;


import java.util.HashMap;
import java.util.Map;


//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Work on 06.07.2016.
 */

public class WebMoneyClient {
    private final String address = "http://92.222.116.219/sendwm/sendwm.php?";
    private String passw;

    private RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper mapper = new ObjectMapper();

    public Map<String, String> makePayment(String wmzWallet, BigDecimal amount, String comment, String fromacc,
                                           String period, String pcode) throws IOException {

        String requestUrl = String.format(address + "passw=%s&destacc=%s&amount=%s&comment=%s&fromacc=%s", passw,
                                            wmzWallet, amount.toString(), comment, fromacc);
        if(period != null) {
            requestUrl += "&period="+period;
            if(pcode != null)
                requestUrl += "&pcode="+pcode;
        }
        String response = restTemplate.getForObject(requestUrl, String.class);

        Map<String, String> map = mapper.readValue(response, new TypeReference<HashMap<String, String>>() {});

//        System.out.println(map);

        return map;
    }

    public Map<String, String> checkBalanсe(String fromacc) throws IOException {
        String requestUrl = String.format(address + "passw=%s&getbal=1&fromacc=%s", passw, fromacc);
        String response = restTemplate.getForObject(requestUrl, String.class);

        Map<String, String> map = mapper.readValue(response, new TypeReference<HashMap<String, String>>() {});
        return map;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }
}