package com.moresise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import static com.moresise.MainController.log;

/**
 * Created by Work on 04.08.2016.
 */
@Component
public class WebMoneyService {

    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final Charset win_1251 = Charset.forName("windows-1251");
    private final String key = "bc65e08beb56cbbfddeb0be7d6f1b440b6679faa174e83f5ad6d043a6fc171ad";

    @Autowired
    ErrorResolver resolver;

    @Autowired
    JsonParser parser;

    public Payaccount convert(String payaccount) throws IOException {
//        System.out.println(payaccount);                 //      Убрать!!!
        return parser.convret(payaccount);
    }

    public Response handlePaymentResponse(Map<String, String> respMap) {
        if(resolver.checkForError(respMap)) {
            return constructError(respMap);
        }
        Response response = new Response(respMap);
        response.setStatus(1);
        response.setFinalstatus(1);
        return response;
    }

    public Response handleCheckResponse(Map<String, String> respMap, BigDecimal amount) {
        if(resolver.checkForError(respMap)) {
            return constructError(respMap);
        }

        Response response;

        BigDecimal bal = new BigDecimal(respMap.get("balance"));
        if(amount.subtract(bal).compareTo(BigDecimal.valueOf(50))>=0){
            respMap.put("result", "success");
            respMap.put("message", "Средств достаточно");
            response = new Response(respMap);
            response.setStatus(1);
        } else if(bal.compareTo(new BigDecimal(0.8).multiply(amount).add(amount)) >= 0) {
            respMap.put("result", "success");
            respMap.put("message", "Средств достаточно");
            response = new Response(respMap);
            response.setStatus(1);
        } else {
            respMap.put("result", "fail");
            respMap.put("message", "Не достаточно средств");
            response = new Response(respMap);
            response.setStatus(-1);
        }

        response.setFinalstatus(1);
        return response;
    }

    public Response constructError(Map<String, String> respMap) {
        resolver.resolveError(respMap);
        Response response = new Response(respMap);
        response.setStatus(-1);
        response.setFinalstatus(-1);
        log.error(response);
        return response;
    }

    public void checkSign(SumOfFields request) throws IncorrectSignatueException {
        String signature = "";
        if(request.getSign()!=null){
            signature = request.sum() + key;
            signature = this.md5Custom(signature);
            if(request.getSign().equals(signature)) {
                return;
            }
        }
        throw new IncorrectSignatueException(signature);
    }

    public static String md5Custom(String st) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while( md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }
}
