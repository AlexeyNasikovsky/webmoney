package com.moresise;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Public on 13.07.2016.
 */
@RestController
@RequestMapping("/")
public class MainController {

    //this controller returns objects i.e. String or a pojo

    public static final Logger log = Logger.getLogger(MainController.class);

    @Autowired
    WebMoneyClient client;

    @Autowired
    WebMoneyService webMoneyService;

    @RequestMapping("/makePayment")
    public Response payment(@RequestBody PaymentRequest request) throws IOException, IncorrectSignatueException {
        log.trace(request.toString());
        webMoneyService.checkSign(request);
        Payaccount payacc = webMoneyService.convert(request.getPayaccount());
        Map<String, String> response = client.makePayment(payacc.getDestacc(), request.getAmount(), request.getOrderid(),
                                                            payacc.getFromacc(), payacc.getPeriod(), payacc.getPcode());
        log.trace(response);
        return webMoneyService.handlePaymentResponse(response);
    }

    @RequestMapping("/checkDetails")
    public Response check(@RequestBody CheckRequest request) throws IOException, IncorrectSignatueException {
        log.trace(request.toString());
        webMoneyService.checkSign(request);
        Payaccount payacc = webMoneyService.convert(request.getPayaccount());
        Map<String, String> response = client.checkBalanсe(payacc.getFromacc());
        log.trace(response);
        return webMoneyService.handleCheckResponse(response, request.getAmount());
    }

    @ExceptionHandler(IOException.class)
    public Response exception(IOException e) {
        Map<String, String> error = new HashMap<String, String>();
        error.put("code", "350");                       //    Такой ли код????????
        log.error(e);
        return webMoneyService.constructError(error);
    }

    @ExceptionHandler(IncorrectSignatueException.class)
    public Response exception(IncorrectSignatueException e) {
        Map<String, String> error = new HashMap<String, String>();
        error.put("code", "351");                       //    Такой ли код????????
        error.put("message", e.getMessage());
        log.error(e);
        return webMoneyService.constructError(error);
    }
}
