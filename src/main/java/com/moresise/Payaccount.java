package com.moresise;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Work on 05.08.2016.
 */
public class Payaccount {
    @JsonIgnoreProperties(ignoreUnknown = true)

    private String destacc;
    private String fromacc;
    private String period;
    private String pcode;

    public String getDestacc() {
        return destacc;
    }

    public void setDestacc(String destacc) {
        this.destacc = destacc;
    }

    public String getFromacc() {
        return fromacc;
    }

    public void setFromacc(String fromacc) {
        this.fromacc = fromacc;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }
}
