package com.moresise;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Work on 03.08.2016.
 */

public class Errors {
    private Map<Integer , String> errMap = new HashMap<Integer, String>();

    public Map<Integer, String> getErrMap() {
        return errMap;
    }

    public void setErrMap(Map<Integer, String> errMap) {
        this.errMap = errMap;
    }
}
