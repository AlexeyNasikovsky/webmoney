package com.moresise;

/**
 * Created by Work on 01.08.2016.
 */

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import static com.moresise.MainController.log;

/**
 <response>
 <status>1</status>
 <desc>Платеж принят </desc>
 <paymentid>496</paymentid> //передается в запросе
 <finalstatus>1</finalstatus>
 <date>2016-07-15 11:58:26</date>
 <sign>99a1f6d51d56b1d69fb8db4675efbb2e</sign>
 </response>
 */

@XmlRootElement(name = "response")
public class Response {
    private int status;
    private String desc;
    private int code;
    private int paymentid;
    private int finalstatus;
    private BigDecimal balance;
    private String result;
    private BigDecimal amount;
    private BigDecimal comiss;
    private Date date;

//    private String sign;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Response(Map<String, String> obj) {
        try {
            date = formatter.parse(formatter.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

        code = Integer.parseInt(obj.get("code"));
        if(obj.containsKey("message"))
            if(obj.get("message")!=null)
                desc = obj.get("message");
        if(obj.containsKey("paymentid")) //                ????????????
            if(obj.get("paymentid")!=null)
                paymentid = Integer.parseInt(obj.get("paymentid"));
        if(obj.containsKey("balance"))
            if(obj.get("balance")!=null)
                balance = new BigDecimal(obj.get("balance"));
        if(obj.containsKey("result"))
            if(obj.get("result")!=null)
                result = obj.get("result");
        if(obj.containsKey("amount"))
            if(obj.get("amount")!=null)
                amount = new BigDecimal(obj.get("amount"));
        if(obj.containsKey("comiss"))
            if(obj.get("comiss")!=null)
                comiss = new BigDecimal(obj.get("comiss"));
    }

//    @Override
//    public String toString() {
//        return String.format("status=%d\ndesc=%s\ncode=%d\npaymentid=%d\nfinalstatus=%d\n" +
//                "balance=%s\nresult=%s\namount=%s\ncomiss=%s\ndate=%s\n",
//                status, desc, code, paymentid, finalstatus, balance.toString(), result,
//                amount.toString(), comiss.toString(), date);
//    }

    public Response() {}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(int paymentid) {
        this.paymentid = paymentid;
    }

    public int getFinalstatus() {
        return finalstatus;
    }

    public void setFinalstatus(int finalstatus) {
        this.finalstatus = finalstatus;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getComiss() {
        return comiss;
    }

    public void setComiss(BigDecimal comiss) {
        this.comiss = comiss;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

//    public String getSign() {
//        return sign;
//    }
//
//    public void setSign(String sign) {
//        this.sign = sign;
//    }

}
