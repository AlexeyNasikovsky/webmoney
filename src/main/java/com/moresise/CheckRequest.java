package com.moresise;

/**
 * Created by Work on 11.07.2016.
 */

import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

/*<checkPaymentDetailsRequest>
//    <sign>feedd6f15ee0f3fff3ad9812d3a3ea7e</sign>
//    <apiid>1</apiid>
//    <method>check</method>
//    <account type="user_account">380983145264</account>
//    <providerid>2</providerid>
//    <amount>1.00</amount>
//    <serviceid>243</serviceid>
//    <description>10201</description>
//    <payaccount>{
//   "account" : "destacc", "type": "wmz"
// }</payaccount>
//    <payaccount2></payaccount2>
// </checkPaymentDetailsRequest>*/

/*<paymentRequest>
   <sign>b0785f8a1b75f47259ad3752015cccee</sign>
   <apiid>1</apiid>
   <method>payment</method>
   <account type="user_account">380983145264</account>
//   <currency>UAH</currency>
//   <orderid>c792f539-baf1-4f49-9ecb-24ad9a5c49ff</orderid>
   <providerid>2</providerid>
   <amount>1.00</amount>
//   <data>10201</data>
   <serviceid>243</serviceid>
   <payaccount>{
  "account" : "destacc", "fromacc": "wmz" }
</payaccount>
   <payaccount2></payaccount2>
</paymentRequest>*/
/**
 * <checkPaymentDetailsRequest>
 * <sign>e8d46b33c40005d4372993f4467019a3</sign>
 * <apiid>1</apiid>
 * <method>check</method>
 * <account type="user_account">380635712901</account>
 * <providerid>2</providerid>
 * <amount>50.00</amount>
 * <serviceid>244</serviceid>
 * <description>380635712901</description>
 * <payaccount>380635712901</payaccount>
 * <payaccount2></payaccount2>
 * </checkPaymentDetailsRequest>
 */
@XmlRootElement(name = "checkPaymentDetailsRequest")
public class CheckRequest implements SumOfFields{
    private String sign;
    private String apiid;
    private String method;
    private String account;
    private Integer providerid;
    private BigDecimal amount;
    private Integer serviceid;
    private Long description;
    private String payaccount;
    private String payaccount2;

    @Override
    public String toString() {
        return String.format("sign=%s\napiid=%s\naccount=%s\nproviderid=%d\namount=%s\n" +
                        "serviceid=%d\ndescription=%d\npayaccount=%s\npayaccount2=%s\n",
                sign, apiid, account, providerid, amount.toString(), serviceid, description,
                payaccount, payaccount2);
    }

    @Override
    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getApiid() {
        return apiid;
    }

    public void setApiid(String apiid) {
        this.apiid = apiid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getProviderid() {
        return providerid;
    }

    public void setProviderid(Integer providerid) {
        this.providerid = providerid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getServiceid() {
        return serviceid;
    }

    public void setServiceid(Integer serviceid) {
        this.serviceid = serviceid;
    }

    public Long getDescription() {
        return description;
    }

    public void setDescription(Long description) {
        this.description = description;
    }

    public String getPayaccount() {
        return payaccount;
    }

    public void setPayaccount(String payaccount) {
        this.payaccount = payaccount;
    }

    public String getPayaccount2() {
        return payaccount2;
    }

    public void setPayaccount2(String payaccount2) {
        this.payaccount2 = payaccount2;
    }

    @Override
    public String sum() {
        String sum = "";
        if(apiid!=null)         sum+=apiid;
        if(method!=null)        sum+=method;
        if(account!=null)       sum+=account;
        if(providerid!=null)    sum+=providerid.toString();
        if(amount!=null)        sum+=amount.toString();
        if(serviceid!=null)     sum+=serviceid.toString();
        if(description!=null)   sum+=description.toString();
        if(payaccount!=null)    sum+=payaccount;
        if(payaccount2!=null)   sum+=payaccount2;
        return sum;
    }
}
