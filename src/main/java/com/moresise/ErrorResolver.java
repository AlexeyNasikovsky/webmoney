package com.moresise;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moresise.MainController.log;
import static com.moresise.WebMoneyService.UTF_8;
import static com.moresise.WebMoneyService.win_1251;
/**
 * Created by Work on 04.08.2016.
 */

public class ErrorResolver {
    private Errors errors = new Errors();

    private List<String> paths = new ArrayList<String>();

    public boolean checkForError(Map<String, String> respMap) {
        if(Integer.parseInt(respMap.get("code"))==0) {
            return false;
        }
        return true;
    }

    public void resolveError(Map<String, String> respMap) {
        Integer code = Integer.parseInt(respMap.get("code"));
        if(code == 351) {
            return;
        }
        if(!errors.getErrMap().containsKey(code)) {
            respMap.clear();
            respMap.put("code", code.toString());
            respMap.put("message", "Неизвестная ошибка: " + respMap.get("message").toString());
            return;
        }
        respMap.clear();

        respMap.put("code", code.toString());
        respMap.put("message", errors.getErrMap().get(code));

        respMap.put("result", "fail");
    }

    public void setNewPath(String newPath) {
        paths.add(newPath);
        Map<Integer, String> errorsMap = new HashMap<Integer, String>();
        for(String path : paths) {
            setErrorSource(path, errorsMap);
        }
        errors.setErrMap(errorsMap);
    }

    private void setErrorSource(String source, Map<Integer, String> errorsMap) {
        String line;

        try {
            InputStream in = new ClassPathResource(source).getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            while((line=reader.readLine()) != null) {
                int code = Integer.parseInt(line.substring(0, line.indexOf(" ")));
                byte ptext[] = line.substring(line.indexOf(" ")).getBytes(win_1251);
                line = new String(ptext, UTF_8);
                errorsMap.put(code, line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Faild to create 'ErrorResolver': resource didn't find\n", e);
        }
    }

    public List<String> getPaths() {
        return paths;
    }
}
