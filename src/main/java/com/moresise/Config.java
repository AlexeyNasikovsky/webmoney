package com.moresise;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Public on 13.07.2016.
 */
@Configuration
@ComponentScan("com.moresise")
@PropertySource("classpath:log4j.properties")
@EnableWebMvc
public class Config extends WebMvcConfigurerAdapter {

    @Autowired
    Environment environment; //use this to insert props

    @Bean
    public WebMoneyClient client(){
        WebMoneyClient client = new WebMoneyClient();
        client.setPassw("GnvZnqmdFovCpq1FvmX3");
        return client; //inject props here
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public UrlBasedViewResolver setupViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        resolver.setOrder(1);
        return resolver;
    }
    @Bean
    public StringHttpMessageConverter stringHttpMessageConverter(){
        return new StringHttpMessageConverter();
    }

//    @Bean
//    public CommonsMultipartResolver multipartResolver() {
//        return new CommonsMultipartResolver();
//    }

//    @Bean
//    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){
//        return new MappingJackson2HttpMessageConverter();
//    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/*")
                .addResourceLocations("/");
        registry.addResourceHandler("/css/*")
                .addResourceLocations("/css/");
        registry.addResourceHandler("/js/*")
                .addResourceLocations("/js/");
        registry.addResourceHandler("/img/*")
                .addResourceLocations("/img/");
    }

    @Bean
    MarshallingHttpMessageConverter marshallingHttpMessageConverter(){
        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter();
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        Class[] toBeBound = {Response.class, PaymentRequest.class, CheckRequest.class};
        marshaller.setClassesToBeBound(toBeBound); //todo this
        marshallingHttpMessageConverter.setMarshaller(marshaller);
        marshallingHttpMessageConverter.setUnmarshaller(marshaller);
        return marshallingHttpMessageConverter;
    }


//    @Bean
//    Service createService(){
//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new InputStreamReader(new ClassPathResource("errorCodes.txt").getInputStream()));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Service obj = new Service(reader);
//        return obj;
//    }

    @Bean
    ErrorResolver resolver(){
        ErrorResolver resolver = new ErrorResolver();
        resolver.setNewPath("errorCodes");
        return resolver;
    }
}
