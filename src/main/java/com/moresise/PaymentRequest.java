package com.moresise;

import javax.xml.bind.annotation.XmlRootElement;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Created by Work on 05.08.2016.
 */

/*<paymentRequest>
   <sign>b0785f8a1b75f47259ad3752015cccee</sign>
   <apiid>1</apiid>
   <method>payment</method>
   <account type="user_account">380983145264</account>
//   <currency>UAH</currency>
//   <orderid>c792f539-baf1-4f49-9ecb-24ad9a5c49ff</orderid>
   <providerid>2</providerid>
   <amount>1.00</amount>
//   <data>10201</data>
   <serviceid>243</serviceid>
   <payaccount>{
  "destacc" : "destacc", "fromacc": "wmz" }
</payaccount>
   <payaccount2></payaccount2>
</paymentRequest>*/
@XmlRootElement(name = "paymentRequest")
public class PaymentRequest implements SumOfFields{
    private String sign;
    private String apiid;
    private String method;
    private String account;
    private String currency;
    private String orderid;
    private Integer providerid;
    private BigDecimal amount;
    private String data;
    private Integer serviceid;
    private Long description;
    private String payaccount;
    private String payaccount2;

    @Override
    public String toString() {
        return String.format("sign=%s\napiid=%s\nmethod=%s\naccount=%s\ncurrency=%s\norderid=%s\nproviderid=%d\namount=%s\n" +
                        "data=%s\nserviceid=%d\ndescription=%d\npayaccount=%s\npayaccount2=%s\n",
                sign, apiid, method, account, currency, orderid, providerid, amount.toString(), data, serviceid, description,
                payaccount, payaccount2);
    }

    @Override
    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getApiid() {
        return apiid;
    }

    public void setApiid(String apiid) {
        this.apiid = apiid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Integer getProviderid() {
        return providerid;
    }

    public void setProviderid(Integer providerid) {
        this.providerid = providerid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getServiceid() {
        return serviceid;
    }

    public void setServiceid(Integer serviceid) {
        this.serviceid = serviceid;
    }

    public Long getDescription() {
        return description;
    }

    public void setDescription(Long description) {
        this.description = description;
    }

    public String getPayaccount() {
        return payaccount;
    }

    public void setPayaccount(String payaccount) {
        this.payaccount = payaccount;
    }

    public String getPayaccount2() {
        return payaccount2;
    }

    public void setPayaccount2(String payaccount2) {
        this.payaccount2 = payaccount2;
    }

    public String sum() {
        String sum = "";
        if(apiid!=null)         sum+=apiid;
        if(method!=null)        sum+=method;
        if(account!=null)       sum+=account;
        if(currency!=null)      sum+=currency;
        if(orderid!=null)       sum+=orderid;
        if(providerid!=null)    sum+=providerid.toString();
        if(amount!=null)        sum+=amount.toString();
        if(data!=null)          sum+=data;
        if(serviceid!=null)     sum+=serviceid.toString();
        if(description!=null)   sum+=description.toString();
        if(payaccount!=null)    sum+=payaccount;
        if(payaccount2!=null)   sum+=payaccount2;
        return sum;
    }
}
